// @flow

import * as React from 'react';
import Dialog from './components/dialog/dialog';
import './app.css';

class App extends React.Component {
  render(): React.Element<any> {
    return (
      <div className="app">
        <Dialog className='app__dialog'/>
      </div>
    );
  }
};

export default App;
