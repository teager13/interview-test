// @flow

import * as React from 'react';
import './user.css';

export type IUser = {
  id: number,
  login: string,
  avatarUrl: string,
}

type Props = {
  user: IUser,
  isSelected: boolean,
  onClick: (user: IUser) => void
}

const User = (props: Props) => {

  this.handleClick = () => {
    props.onClick(props.user, props.isSelected);
  };

  return (
    <div className={`user user--list ${props.isSelected ? 'user--selected' : ''}`} onClick={!props.isSelected ? this.handleClick : null}>
      <img className='user__avatar'src={props.user.avatarUrl} alt="" />
        <span className='user__name'>{props.user.login}</span>
      { props.isSelected && <button className='user__button' onClick={this.handleClick}>X</button> }
    </div>
  );
};

export default User;