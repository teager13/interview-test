// @flow

import type {IUser} from '../../models';

const URL = 'https://api.github.com/users?since=135';

export type IDialogActions = {
  getUsers: () => Promise<void>,
  selectUsers: (user: IUser) => void,
  clearSelectUsers: () => void
}

class DialogActions {

  constructor(dispatch){
    this.dispatch = dispatch;
  }

  getUsers = () => {
    const disp = this.dispatch;
    disp({
      type: 'GET_USERS_REQUEST'
    });
    return fetch(URL)
      .then((response) => response.json())
      .then((response) => {

          const users = response.map(u => {
            return {
              id: u.id,
              login: u.login,
              avatarUrl: u.avatar_url,
              isSelected: false
            }
          });

          disp({
            type: 'GET_USERS_SUCCESS',
            payload: users
          });
        }
      );
  };

  selectUsers = (users: Array<IUser>) => {
    this.dispatch({
      type: 'SELECT_USERS',
      payload: users
    });
  };

  clearSelectUsers = () => {
    this.dispatch({
      type: 'CLEAR_SELECT_USERS',
    });
  };
};

export default DialogActions;