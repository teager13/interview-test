// @flow

import * as React from 'react';
import {connect} from 'react-redux';
import User from '../user/user';
import type {IUser} from '../user/user';
import DialogActions from './dialog-actions';
import type {IDialogActions} from './dialog-actions';
import './dialog.css';

type Props = {
  className: string,
  users: Array<IUser>,
  isLoading: boolean,
  selectedUsers: Array<IUser>,
  actions: IDialogActions

}

type State = {
  users: Array<IUser>,
  selectedUsers: Array<IUser>,
  searchUser: string
}

class Dialog extends React.Component<Props, State> {

  constructor(props) {
    super(props);
    this.state = {
      users: this.props.users || [],
      selectedUsers: this.props.selectedUsers || [],
      searchUser: ''
    };
    this.handleSelectUser = this.handleSelectUser.bind(this);
    this.handleChangeOption = this.handleChangeOption.bind(this);
    this.handleSaveSelectedUsers = this.handleSaveSelectedUsers.bind(this);
    this.handleClearSelectedUsers = this.handleClearSelectedUsers.bind(this);
  }

  async componentWillMount() {
    await this.props.actions.getUsers();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      users: nextProps.users
    });
  }

  handleSelectUser = (user: any, isSelected: boolean) => {
    if (isSelected) {
      this.setState((prevState) => {
        return {
          selectedUsers: prevState.selectedUsers.filter(u => u.id !== user.id),
          users: [...prevState.users, user]
        }
      });
    } else {
      this.setState((prevState) => {
        return {
          users: prevState.users.filter(u => u.id !== user.id),
          selectedUsers: [...prevState.selectedUsers, user]
        }
      });
    }
  };

  handleChangeOption = (e: any) => {
    this.setState({
      searchUser: e.target.value
    });
  };

  handleSaveSelectedUsers = () => {
    this.props.actions.selectUsers(this.state.selectedUsers);
    this.setState({
      selectedUsers: []
    });
  };

  handleClearSelectedUsers = () => {
    this.props.actions.clearSelectUsers();
    this.setState({
      selectedUsers: []
    });
  };

  renderSelectedUsers() {
    let template;

    if (this.state.selectedUsers.length) {
      template = this.state.selectedUsers.map((u, index) => {
        return <User key={index} user={u} isSelected={true} onClick={this.handleSelectUser}/>;
      });
    } else {
      template = null;
    }
    return <div className='dialog__selected-users'>
      {template}
    </div>;
  }

  renderUsersList() {
    const {isLoading} = this.props;
    if(isLoading) {
      return (
        <div className='dialog__list'>
          <div className="empty-list">Загрузка...</div>
        </div>
      );
    }

    const {searchUser, users} = this.state;
    let template;
    let searchUsers = [];

    if(users.length) {
      searchUsers = users.filter(u => {
        if (searchUser) {
          const itemName = u.login.toLocaleLowerCase();
          return itemName.includes(searchUser.trim());
        }
        return true;
      });
    }

    if (searchUsers.length) {
      template = searchUsers.map((u, index) => {
        return <User key={index} user={u} onClick={this.handleSelectUser}/>;
      });
    } else {
      template = <div className="empty-list">Нет данных</div>;;
    }

    return <div className='dialog__list'>
      {template}
    </div>;
  }

  render() {
    return (
      <section className={`dialog ${this.props.className}`}>
        <div className='dialog__title'>
          <div>Права доступа</div>
        </div>
        <input className='dialog__search' type="text" value={this.state.searchUser} onChange={this.handleChangeOption.bind(this)}/>
        {this.renderSelectedUsers()}
        {this.renderUsersList()}
        <div className='dialog__footer'>
          <button className="dialog__button dialog__button--primary" onClick={this.handleSaveSelectedUsers} >Сохранить</button>
          <button className="dialog__button" onClick={this.handleClearSelectedUsers}>Очистить</button>
        </div>
      </section>
    );
  }
};

const mapStateToProps = (state) => {
  const selectedUsers = state.selectedUsers;
  const selectedUsersId = selectedUsers.map(u => u.id);
    return {
    users: state.users.filter(u => !selectedUsersId.includes(u.id)),
    isLoading: state.isLoading,
    selectedUsers: selectedUsers
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: new DialogActions(dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dialog);

