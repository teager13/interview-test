const initialState = {
  users: [],
  isLoading: false,
  selectedUsers: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_USERS_REQUEST':
      return {
        ...state,
        isLoading: true
      };
    case 'GET_USERS_SUCCESS':
      return {
        ...state,
        users: action.payload,
        isLoading: false
      };
    case 'SELECT_USERS':
      return {
        ...state,
        selectedUsers: [...state.selectedUsers, ...action.payload]
      };
    case 'CLEAR_SELECT_USERS':
      return {
        ...state,
        selectedUsers: []
      };
    default:
      return state;
  }
};

export {initialState, reducer};